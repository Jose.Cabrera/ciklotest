///<reference path="app.component.ts"/>
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from "./app.component";
import {HttpClientModule} from '@angular/common/http';
import {routing} from "./app.routing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ClientComponent} from "./clients/client.component";
import {ClientsComponent} from "./clients/clients";
import {ClientInputComponent} from "./clients/client-input.component";
import {ClientListComponent} from "./clients/client-list.component";
import {HeaderComponent} from "./header.component";
import {ClientService} from "./clients/client.service";
import {ErrorService} from "./errors/error.service";
import {ErrorComponent} from "./errors/error.component";

@NgModule({
    declarations: [
        AppComponent,
        ClientComponent,
        ClientsComponent,
        ClientInputComponent,
        ClientListComponent,
        HeaderComponent,
        ErrorComponent
    ],
    imports: [BrowserModule,
        HttpClientModule,
        routing,
        FormsModule,
        ReactiveFormsModule],
    bootstrap: [AppComponent],
    providers: [
        ClientService,
        ErrorService
    ]
})
export class AppModule {

}