import { Routes, RouterModule } from "@angular/router";
import {ClientsComponent} from "./clients/clients";

const APP_ROUTES: Routes = [
    { path: '', redirectTo: '/clients', pathMatch: 'full' },
    { path: 'clients', component: ClientsComponent }
];

export const routing = RouterModule.forRoot(APP_ROUTES);