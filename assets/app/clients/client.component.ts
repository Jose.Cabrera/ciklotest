import {Component, Input} from "@angular/core";

import {ClientService} from "./client.service";
import {Client} from "./client.model";

@Component({
    selector: 'app-client',
    templateUrl: './client.component.html',
    styles: [`
        .author {
            display: inline-block;
            font-style: italic;
            font-size: 12px;
            width: 80%;
        }

        .config {
            display: inline-block;
            text-align: right;
            font-size: 12px;
            width: 19%;
        }
    `]
})
export class ClientComponent {
    @Input() client: Client;

    constructor(private clientService: ClientService) {
    }

    onEdit() {
        this.clientService.editClient(this.client);
    }

    onDelete() {
        this.clientService.deleteClient(this.client)
            .subscribe(
                result => console.log(result)
            );
    }
}