import {Component, OnInit} from "@angular/core";

import {ClientService} from "./client.service";
import {Client} from "./client.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-client-input',
    templateUrl: './client-input.component.html'
})
export class ClientInputComponent implements OnInit {
    client: Client;
    myForm: FormGroup;

    constructor(private clientService: ClientService) {
    }

    onSubmit() {
        if (this.client) {
            // Edit
            this.client.name = this.myForm.value.name;
            this.client.lastname = this.myForm.value.lastName;
            this.client.age = this.myForm.value.age;
            this.client.email = this.myForm.value.email;
            this.client.address = this.myForm.value.address;
            this.clientService.updateClient(this.client)
                .subscribe(
                    result => {
                        console.log(result);
                        this.myForm.reset();
                    }
                );
            this.client = null;
        } else {
            // Create
            const client = new Client(this.myForm.value.name, this.myForm.value.lastName, this.myForm.value.age, this.myForm.value.email, this.myForm.value.address);
            this.clientService.addClient(client)
                .subscribe(
                    data => {
                        console.log(data);
                        this.myForm.reset();
                    },
                    error => console.error(error)
                );
        }
    }

    onClear() {
        this.client = null;
        this.myForm.reset();
    }

    ngOnInit() {
        this.clientService.clientIsEdit.subscribe(
            (client: Client) => {
                this.client = client;

                this.myForm.patchValue({
                    name: this.client.name,
                    lastName: this.client.lastname,
                    age: this.client.age,
                    email: this.client.email,
                    address: this.client.address
                })
            }
        );
        this.myForm = new FormGroup({
            name: new FormControl(null, Validators.required),
            lastName: new FormControl(null, Validators.required),
            age: new FormControl(null, [
                Validators.required,
                Validators.maxLength(3),
                Validators.min(1),
                Validators.max(100),
                Validators.pattern('^(0|[1-9][0-9]*)$')
            ]),
            email: new FormControl(null, [
                Validators.required,
                Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            ]),
            address: new FormControl()
        });
    }
}