import { Component, OnInit } from "@angular/core";

import { Client } from "./client.model";
import { ClientService } from "./client.service";

@Component({
    selector: 'app-client-list',
    template: `
        <div class="col-md-8 col-md-offset-2">
            <app-client
                   [client]="client"
                    *ngFor="let client of clients"></app-client>
        </div>
    `
})
export class ClientListComponent implements OnInit {
    clients: Client[];

    constructor(private clientService: ClientService) {}

    ngOnInit() {
        this.clientService.getClients()
            .subscribe(
                (clients: Client[]) => {
                    this.clients = clients;
                }
            );
    }
}