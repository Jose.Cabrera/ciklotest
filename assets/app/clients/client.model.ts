export class Client {
    name: string;
    lastname: string;
    age: number;
    email:string;
    address?: string;
    clientId?: string;

    constructor(name: string, lastname: string, age: number, email:string, address?: string, clientId?: string) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.email = email;
        this.address = address;
        this.clientId = clientId;
    }
}