import {Injectable, EventEmitter} from "@angular/core";
import 'rxjs/Rx';
import {Observable} from "rxjs";

import {Client} from "./client.model";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {ErrorService} from "../errors/error.service";

@Injectable()
export class ClientService {
    private clients: Client[] = [];
    clientIsEdit = new EventEmitter<Client>();

    constructor(private http: HttpClient, private errorService: ErrorService) {
    }

    addClient(client: Client) {
        const body = JSON.stringify(client);
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http.post('http://localhost:3000/client', body, {headers: headers})
            .map((response: HttpResponse<any>) => {
                const result = response;
                const client = new Client(result.obj.name, result.obj.lastname, result.obj.age, result.obj.email, result.obj.address, result.obj._id);
                this.clients.push(client);
                return client;
            })
            .catch((error: Response) => {
                this.errorService.handleError(error.error);
                return Observable.throw(error);
            });
    }

    getClients() {
        return this.http.get('http://localhost:3000/client')
            .map((response: HttpResponse<any>) => {
                const clients = response.obj;
                let transformedClients: Client[] = [];
                for (let client of clients) {
                    transformedClients.push(new Client(client.name, client.lastname, client.age, client.email, client.address, client._id));
                }
                this.clients = transformedClients;
                return transformedClients;
            })
            .catch((error: Response) => {
                this.errorService.handleError(error.error);
                return Observable.throw(error);
            });
    }

    editClient(client: Client) {
        console.log(client);
        this.clientIsEdit.emit(client);
    }

    updateClient(client: Client) {
        const body = JSON.stringify(client);
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http.patch('http://localhost:3000/client/' + client.clientId, body, {headers: headers})
            .map((response: HttpResponse<any>) => response)
            .catch((error: Response) => {
                this.errorService.handleError(error.error);
                return Observable.throw(error);
            });
    }

    deleteClient(client: Client) {
        this.clients.splice(this.clients.indexOf(client), 1);
        return this.http.delete('http://localhost:3000/client/' + client.clientId)
            .map((response: HttpResponse<any>) => response)
            .catch((error: Response) => {
                this.errorService.handleError(error.error);
                return Observable.throw(error);
            });
    }
}