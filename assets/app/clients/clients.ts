import { Component } from "@angular/core";

@Component({
    selector: 'app-clients',
    template: `
        <div class="row">
            <app-client-input></app-client-input>
        </div>
        <hr>
        <div class="row">
            <app-client-list></app-client-list>
        </div>
    `
})
export class ClientsComponent {

}